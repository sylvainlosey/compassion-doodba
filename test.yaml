# Development environment that extends common.yaml and imitates production, but isolated
# See more details here: https://github.com/Tecnativa/doodba#testing
#
# ! Make sure to run globalwhitelist.yaml and inverseproxy.yaml before using !
# ! Also add secrets: https://github.com/Tecnativa/doodba#adding-secrets !

version: "2.4"

services:
  odoo:
    extends:
      file: common.yaml
      service: odoo
    env_file:
      - .docker/odoo.env
      - .docker/db-access.env
    environment:
      DOODBA_ENVIRONMENT: "${DOODBA_ENVIRONMENT-test}"
      # To install demo data export DOODBA_WITHOUT_DEMO=false
      WITHOUT_DEMO: "${DOODBA_WITHOUT_DEMO-all}"
      SMTP_PORT: "1025"
      SMTP_SERVER: smtplocal
    restart: unless-stopped
    depends_on:
      - db
      - smtp
    networks:
      default:
      globalwhitelist_shared:
      inverseproxy_shared:
    # Make sure to define $DOMAIN_TEST correctly, traefik will block all other hosts.
    labels:
      traefik.frontend.headers.customResponseHeaders: "X-Robots-Tag:noindex, nofollow"
      traefik.longpolling.frontend.rule: "Host:${DOMAIN_TEST};PathPrefix:/longpolling/"
      traefik.www.frontend.rule: "Host:${DOMAIN_TEST}"
    command:
      - odoo
      - --workers=2
      - --max-cron-threads=1

  db:
    extends:
      file: common.yaml
      service: db
    env_file:
      - .docker/db-creation.env
    restart: unless-stopped

  smtp:
    image: mailhog/mailhog
    restart: unless-stopped
    networks:
      default:
        aliases:
          - smtplocal
      inverseproxy_shared:
    labels:
      traefik.docker.network: "inverseproxy_shared"
      traefik.enable: "true"
      traefik.frontend.passHostHeader: "true"
      traefik.frontend.rule: "Host:${DOMAIN_TEST};PathPrefixStrip:/smtpfake/"
      traefik.port: "8025"
    volumes:
      - "smtpconf:/etc/mailhog:ro,z"
    entrypoint: [sh, -c]
    command:
      - test -r /etc/mailhog/auth && export MH_AUTH_FILE=/etc/mailhog/auth; exec MailHog

networks:
  default:
    internal: true
    driver_opts:
      encrypted: 1

  # Defined by globalwhitelist.yaml and inversepxoy.yaml => external
  globalwhitelist_shared:
    external: true

  inverseproxy_shared:
    external: true

volumes:
  filestore:
  db:
  smtpconf:
