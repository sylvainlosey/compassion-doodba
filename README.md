[![Doodba deployment](https://img.shields.io/badge/deployment-doodba-informational)](https://github.com/Tecnativa/doodba)
[![Last template update](https://img.shields.io/badge/last%20template%20update-v0.1.0-informational)](https://github.com/Tecnativa/doodba-copier-template/tree/v0.1.0)
[![Odoo](https://img.shields.io/badge/odoo-v11-a3478a)](https://github.com/odoo/odoo/tree/11)
[![pipeline status](https://gitlab.com/sylvainlosey/compassion-doodba/badges/11/pipeline.svg)](https://gitlab.com/sylvainlosey/compassion-doodba/commits/11)
[![coverage report](https://gitlab.com/sylvainlosey/compassion-doodba/badges/11/coverage.svg)](https://gitlab.com/sylvainlosey/compassion-doodba/commits/11)
[![AGPL-3.0-or-later license](https://img.shields.io/badge/license-AGPL--3.0--or--later-success})](LICENSE)

# compassion-docker - a Doodba deployment

This project is a Doodba scaffolding. Check upstream docs on the matter:

- [General Doodba docs](https://github.com/Tecnativa/doodba).
- [Doodba copier template docs](https://github.com/Tecnativa/doodba-copier-template)
- [Doodba QA docs](https://github.com/Tecnativa/doodba-qa)

# Credits

This project is maintained by:

[![Tecnativa](https://www.tecnativa.com/r/H3p)](https://www.tecnativa.com/r/bb4)

Also, special thanks to
[our dear community contributors](https://github.com/Tecnativa/doodba-copier-template/graphs/contributors).
